﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{   
    public float speed;
    private Rigidbody2D rbd2d;

    void Start()
    {
        rbd2d = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        Vector2 movement = new Vector2(moveHorizontal, moveVertical);

        rbd2d.AddForce(movement * speed);
 
    }

    void OnTriggerEnter2D(Collider2D other ){
        if(other.gameObject.CompareTag("PickUp")){
            other.gameObject.SetActive(false);
        }
    }
}
 